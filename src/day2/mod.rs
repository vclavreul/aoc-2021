use std::num::ParseIntError;
use std::str::FromStr;

use crate::DayData;

#[derive(Debug)]
enum Direction {
    Forward,
    Up,
    Down,
}

impl FromStr for Direction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "forward" => Ok(Direction::Forward),
            "up" => Ok(Direction::Up),
            "down" => Ok(Direction::Down),
            _ => Err(format!("Invalid direction {}", s))
        }
    }
}

#[derive(Debug)]
pub(crate) struct Day2Data(Vec<(Direction, usize)>);

impl DayData for Day2Data {
    fn from_str(data: &str) -> Result<Self, String> where Self: Sized {
        let mut vec_data = vec![];
        for line in data.lines() {
            let parts: Vec<&str> = line.split_ascii_whitespace().collect();
            if parts.len() != 2 {
                return Err(format!("Invalid input {}", line));
            }

            let direction: Direction = parts.get(0)
                .ok_or("Missing part 0".to_string())?
                .parse()?;
            let steps: usize = parts.get(1)
                .ok_or("Missing part 0".to_string())?
                .parse().map_err(|e: ParseIntError| e.to_string())?;

            vec_data.push((direction, steps));
        }

        Ok(Day2Data(vec_data))
    }

    fn solve_part1(&mut self) -> String {
        let mut x_accumulator = 0;
        let mut y_accumulator = 0;

        for (direction, steps) in &self.0 {
            match direction {
                Direction::Forward => x_accumulator += steps,
                Direction::Up => y_accumulator -= steps,
                Direction::Down => y_accumulator += steps,
            }
        }

        format!("Position is {:?}, result is {}", (x_accumulator, y_accumulator), x_accumulator * y_accumulator)
    }

    fn solve_part2(&mut self) -> String {
        let mut aim: isize = 0;
        let mut x_accumulator:isize = 0;
        let mut y_accumulator:isize = 0;

        for (direction, steps) in &self.0 {
            let isize_steps = *steps as isize;

            match direction {
                Direction::Forward => { x_accumulator += isize_steps; y_accumulator += aim * isize_steps },
                Direction::Up => aim -= isize_steps,
                Direction::Down => aim += isize_steps,
            }
        }

        format!("Position is {:?}, result is {}", (x_accumulator, y_accumulator), x_accumulator * y_accumulator)
    }
}