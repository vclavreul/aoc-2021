use crate::DayData;

#[derive(Debug)]
pub(crate) struct Day3Data(Vec<[bool; 12]>);

impl Day3Data {
    pub fn take_data(self) -> Vec<[bool; 12]> {
        self.0
    }
}

impl DayData for Day3Data {
    fn from_str(data: &str) -> Result<Self, String> where Self: Sized {
        let mut parsed_data = vec![];
        for line in data.lines() {
            let bool_vec: Vec<bool> = line.chars()
                .map(|c| match c {
                    '0' => Ok(false),
                    '1' => Ok(true),
                    _ => Err("Invalid char".to_string()),
                })
                .collect::<Result<_, String>>()?;

            let bool_array: [bool; 12] = bool_vec.try_into()
                .map_err(|_| "Invalid length".to_string())?;

            parsed_data.push(bool_array);
        }

        Ok(Day3Data(parsed_data))
    }

    fn solve_part1(&mut self) -> String {
        let mut gamma_rate = [false; 12];
        let mut epsilon_rate = [false; 12];

        for i in 0..12 {
            if get_most_common_bit(&self.0, i) {
                gamma_rate[i] = true;
            } else {
                epsilon_rate[i] = true;
            }
        }

        let gamma_value = convert_binary_into_usize(&gamma_rate);
        let epsilon_value = convert_binary_into_usize(&epsilon_rate);

        format!("{:?}", gamma_value * epsilon_value)
    }

    fn solve_part2(&mut self) -> String {
        let o2_bool_array = get_similar_bool_array(true,&self.0);
        let co2_bool_array = get_similar_bool_array(false, &self.0);

        let o2_rate = convert_binary_into_usize(&o2_bool_array);
        let co2_rate = convert_binary_into_usize(&co2_bool_array);

        format!("{}", o2_rate * co2_rate)
    }
}

fn convert_binary_into_usize<const N: usize>(input: &[bool; N]) -> usize {
    let mut output: usize = 0;

    for i in 0..N {
        let val = input[N - 1 - i];
        if val {
            output += 2_usize.pow(i as u32);
        }
    }

    output
}

fn get_most_common_bit<const N: usize>(input: &Vec<[bool; N]>, index: usize) -> bool {
    let mut count_true: usize = 0;
    let mut count_false: usize = 0;
    for bool_array in input {
        match bool_array[index] {
            true => count_true += 1,
            false => count_false += 1,
        }
    }

    count_true >= count_false
}

fn get_similar_bool_array<const N: usize>(use_most_common: bool, input: &Vec<[bool; N]>) -> [bool; N] {
    let mut rating = input.clone();
    for i in 0..N {
        let most_common_bit = get_most_common_bit(&rating, i);
        if rating.len() <= 1 {
            break;
        }
        if use_most_common {
            rating = rating.into_iter()
                .filter(|bool_array| bool_array[i] == most_common_bit)
                .collect();
        } else {
            rating = rating.into_iter()
                .filter(|bool_array| bool_array[i] != most_common_bit)
                .collect();
        }
    }

    rating.get(0).unwrap().clone()
}

#[cfg(test)]
mod test {
    use crate::day3::{convert_binary_into_usize, get_similar_bool_array};

    #[test]
    fn test_part2() {
        let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
        let mut data = vec![];
        for line in input.lines() {
            let bool_vec: Vec<bool> = line.chars()
                .map(|c| match c {
                    '0' => Ok(false),
                    '1' => Ok(true),
                    _ => Err("Invalid char".to_string()),
                })
                .collect::<Result<_, String>>()
                .unwrap();

            let bool_array: [bool; 5] = bool_vec.try_into()
                .unwrap();

            data.push(bool_array);
        }

        let o2_bool_array = get_similar_bool_array(true, &data);
        let co2_bool_array = get_similar_bool_array(false, &data);

        let o2_rate = convert_binary_into_usize(&o2_bool_array);
        let co2_rate = convert_binary_into_usize(&co2_bool_array);

        assert_eq!(23, o2_rate, "O2 rate");
        assert_eq!(10, co2_rate, "CO2 rate");
    }
}