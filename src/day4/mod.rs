use std::num::ParseIntError;
use std::str::FromStr;

use crate::DayData;

#[derive(Debug)]
pub(crate) struct Day4Data {
    input: Vec<usize>,
    boards: Vec<Board>,
}

#[derive(Debug, Clone)]
pub(crate) struct Board([[Cell; 5]; 5]);

#[derive(Debug, Clone)]
struct Cell(usize, bool);

impl DayData for Day4Data {
    fn from_str(data: &str) -> Result<Self, String> where Self: Sized {
        let mut input = vec![];
        let mut boards = vec![];
        let mut board_data = vec![];

        for (line_index, line_data) in data.lines().enumerate() {
            if line_index == 0 {
                input = line_data.split(",")
                    .map(|part| part.parse())
                    .collect::<Result<Vec<usize>, ParseIntError>>()
                    .map_err(|e| e.to_string())?;
                continue;
            }

            if line_data == "" && board_data.len() > 0 {
                boards.push(Board(
                    board_data.try_into()
                        .map_err(|_| format!("Invalid board length at line {}", line_index))?
                ));
                board_data = vec![];
                continue;
            }

            if line_data != "" {
                let mut board_line = vec![];
                for part in line_data.split_whitespace() {
                    let value: usize = part.parse()
                        .map_err(|e: ParseIntError| e.to_string())?;

                    let cell: Cell = value.into();
                    board_line.push(cell);
                }

                board_data.push(board_line
                    .try_into()
                    .map_err(|_| format!("Invalid board line length at line {}", line_index))?
                )
            }
        }

        Ok(Day4Data { input, boards })
    }

    fn solve_part1(&mut self) -> String {
        let (value, boards) = self.match_input_until_winners();
        if boards.len() != 1 || !value.is_some() {
            return "No result...".to_string();
        }

        format!("{}", boards.get(0).unwrap().get_score(value.unwrap()))
    }

    fn solve_part2(&mut self) -> String {
        todo!()
    }
}

impl Day4Data {
    pub fn match_input_until_winners(&mut self) -> (Option<usize>, Vec<Board>) {
        for value in self.input.clone() {
            let winners = self.match_all_value(value);
            if winners.len() > 0 {
                return (Some(value), winners);
            }
        }

        (None, vec![])
    }

    pub fn match_all_value(&mut self, value: usize) -> Vec<Board> {
        let mut winners = vec![];
        for board in &mut self.boards {
            board.match_value(value);
            if board.is_valid() {
                winners.push(board.clone());
            }
        }

        winners
    }
}

impl Board {
    fn match_value(&mut self, value: usize) {
        for i in 0..5 {
            for j in 0..5 {
                if &self.0[i][j].0 == &value {
                    self.0[i][j].1 = true;
                }
            }
        }
    }

    fn is_valid(&self) -> bool {
        for i in 0..5 {
            let mut is_row_full = true;
            let mut is_column_full = true;
            for j in 0..5 {
                is_row_full = is_row_full && *&self.0[i][j].1;
                is_column_full = is_column_full && *&self.0[j][i].1;
            }

            if is_row_full || is_column_full {
                return true;
            }
        }

        false
    }

    fn get_score(&self, value: usize) -> usize {
        let mut score = 0;
        for i in 0..5 {
            for j in 0..5 {
                if !self.0[i][j].1 {
                    score += self.0[i][j].0;
                }
            }
        }

        score * value
    }
}

impl From<usize> for Cell {
    fn from(value: usize) -> Self {
        Cell(value, false)
    }
}

#[cfg(test)]
mod tests {
    use crate::core::DayData;
    use crate::Day4Data;

    #[test]
    fn basic_test_part1() {
        let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7

";

        let result_data = Day4Data::from_str(input);
        assert!(result_data.is_ok());

        let mut data = result_data.unwrap();
        let (win_value, winners) = data.match_input_until_winners();

        assert_eq!(1, winners.len());
        assert!(win_value.is_some());
        assert_eq!(24, win_value.unwrap());
        assert_eq!(4512, winners[0].get_score(win_value.unwrap()));
    }
}