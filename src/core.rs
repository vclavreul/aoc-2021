use std::fmt::Debug;

use structopt::StructOpt;

#[derive(Debug, StructOpt, PartialEq)]
pub(crate) enum AocDay {
    Day1,
    Day2,
    Day3,
    Day4,
}

pub(crate) trait DayData: Debug {
    fn from_str(data: &str) -> Result<Self, String> where Self: Sized;
    fn solve_part1(&mut self) -> String;
    fn solve_part2(&mut self) -> String;
}