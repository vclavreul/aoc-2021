use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use structopt::StructOpt;

use crate::core::{AocDay, DayData};
use crate::day1::Day1Data;
use crate::day2::Day2Data;
use crate::day3::Day3Data;
use crate::day4::Day4Data;

mod core;
mod day1;
mod day2;
mod day3;
mod day4;


#[derive(Debug, StructOpt)]
#[structopt(name = "Avent of Code 2021")]
pub(crate) struct MainOptions {
    #[structopt(subcommand)]
    pub day: AocDay,

    #[structopt(short, long, parse(from_os_str))]
    pub input_file: PathBuf,
}

fn main() -> Result<(), String> {
    let opt = MainOptions::from_args();
    println!("Advent of Code // {:?}", &opt.day);

    println!("=> Reading {:?}", &opt.input_file);
    let mut file = File::open(&opt.input_file)
        .map_err(|e| e.to_string())?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|e| e.to_string())?;

    let mut day_data = build_day_data(contents.as_str(), &opt.day)?;

    println!("- solution part 1: {}", day_data.solve_part1());
    println!("- solution part 2: {}", day_data.solve_part2());

    Ok(())
}

fn build_day_data(str_data: &str, aoc_day: &AocDay) -> Result<Box<dyn DayData>, String> {
    Ok(match aoc_day {
        AocDay::Day1 => Box::new(Day1Data::from_str(str_data)?),
        AocDay::Day2 => Box::new(Day2Data::from_str(str_data)?),
        AocDay::Day3 => Box::new(Day3Data::from_str(str_data)?),
        AocDay::Day4 => Box::new(Day4Data::from_str(str_data)?),
    })
}
