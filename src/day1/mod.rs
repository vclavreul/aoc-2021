use std::num::ParseIntError;

use crate::core::{DayData};

#[derive(Debug)]
pub(crate) struct Day1Data(Vec<usize>);

impl DayData for Day1Data {
    fn from_str(s: &str) -> Result<Self, String> {
        s.lines()
            .map(|line_data| line_data.parse())
            .collect::<Result<Vec<usize>, ParseIntError>>()
            .map(|vec_data| Day1Data(vec_data))
            .map_err(|parse_err| parse_err.to_string())
    }

    fn solve_part1(&mut self) -> String {
        self.0.windows(2)
            .filter(|data| data[0] < data[1])
            .count().to_string()
    }

    fn solve_part2(&mut self) -> String {
        self.0.windows(3)
            .map(|data| data[0] + data[1] + data[2])
            .collect::<Vec<usize>>()
            .windows(2)
            .filter(|data| data[0] < data[1])
            .count().to_string()
    }
}